/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.stevekane.android.test.testpushnotifications;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.stevekane.android.test.testpushnotifications";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 2;
  public static final String VERSION_NAME = "";
  /**
   * @deprecated Use {@link #APPLICATION_ID}
   */
  @Deprecated
  public static final String PACKAGE_NAME = "com.stevekane.android.test.testpushnotifications";
}
