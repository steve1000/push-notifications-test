package com.stevekane.android.test.testpushnotifications;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class PushNotificationResult extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_push_result);
		
		Intent i = getIntent();
		String message = i.getStringExtra("Notice");
		String jobId = i.getStringExtra("jobId");

		TextView tv1 = (TextView) findViewById(R.id.message);
		TextView tv2 = (TextView) findViewById(R.id.jobId);
		
		tv1.setText(message);
		tv2.setText(jobId);
		
		
	}
	
}
